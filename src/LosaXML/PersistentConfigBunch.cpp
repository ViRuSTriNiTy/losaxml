// This file is part of the LosaXML library.
// Copyright (C) 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LOSAXML_PERSISTENTCONFIGBUNCH_CPP_
#define LOSAXML_PERSISTENTCONFIGBUNCH_CPP_

#include <LosaXML/PersistentConfigBunch.h>
#include <LosaXML/DOMDocument.h>
#include <LosaXML/Utils.h> // for convertStr<>()
#include <boost/scoped_ptr.hpp>
#include <iostream>

using namespace LosaXML;

template<class I>
CPersistentConfigBunch<I>::CPersistentConfigBunch(CPersistentConfigItem * apParent, std::wstring aXmlNodeName, CPersistentConfigBunchItemFactory<I> & aItemFactory)
  : inherited(apParent, aXmlNodeName)
  , mItemFactory(aItemFactory)
  , mItems()
{}

template<class I>
CPersistentConfigBunch<I>::~CPersistentConfigBunch()
{
  for (I * i: mItems)
    delete i;

  mItems.clear();
}

template<class I>
CPersistentConfigBunch<I>::CPersistentConfigBunch(const CPersistentConfigBunch<I> & aSource)
  : inherited(aSource)
  , mItemFactory(aSource.mItemFactory)
  , mItems()
{
  assign(aSource);
}

template<class I>
CPersistentConfigBunch<I> & CPersistentConfigBunch<I>::operator= (const CPersistentConfigBunch<I> & aSource)
{
  if (this == &aSource)
    return *this;

  inherited::operator=(aSource);

  assign(aSource);

  return *this;
}

template<class I>
unsigned int CPersistentConfigBunch<I>::size() const
{
  return mItems.size();
}

template<class I>
void CPersistentConfigBunch<I>::assign(const CPersistentConfigBunch<I> & aSource)
{
  std::wstring lErrorMessage(L"");
  typename std::list<I *>::iterator i(mItems.begin());
  for (I * s: aSource.mItems)
  {
    if (i == mItems.end())
    {
      I * lItem(mItemFactory.createItem(this));

      *lItem = *s;

      if (add(lItem, lErrorMessage))
      {
        i = mItems.end();
      }
      else
      {
        delete lItem;

        std::wcout << "Error in CPersistentConfigBunch<I>::assign(), " << lErrorMessage << std::endl << std::flush;
      }
    }
    else
    {
      **i = *s;

      ++i;
    }
  }

  // now remove items that are not needed anymore because source bunch has less items
  while (aSource.mItems.size() < mItems.size())
  {
    I * i(mItems.back());

    remove(i);
  }
}

template<class I>
void CPersistentConfigBunch<I>::reset()
{
  while (!mItems.empty())
  {
    I * i(mItems.front());

    remove(i);
  }

  inherited::reset();
}

template<class I>
bool CPersistentConfigBunch<I>::afterReadFromXml(CDOMNode & arConfigNode, std::wstring & arErrorMessage)
{
  // note: no inherited call because inherited behavior does not work with multiple items having the same XML node name

  // get child configuration node name
  boost::scoped_ptr<I> lXmlNodeNameHelper(mItemFactory.createItem(this));

  CXMLNodeList lNodes;
  if (arConfigNode.getNodes(lXmlNodeNameHelper->getXmlNodeName(), lNodes))
  {
    for (CDOMNode & n: lNodes)
    {
      I * i(mItemFactory.createItem(this));
      if (i->readFromXml(n, false, arErrorMessage))
      {
        if (!add(i, arErrorMessage))
        {
          delete i;

          return false;
        }
      }
      else
      {
        delete i;

        return false;
      }
    }
  }

  return true;
}

template<class I>
bool CPersistentConfigBunch<I>::afterWriteToXml(CDOMNode & arConfigNode, std::wstring & arErrorMessage)
{
  if (mItems.size() > 0)
  {
    CXMLNodeList lNodeList;
    arConfigNode.getNodes(mItems.front()->getXmlNodeName(), lNodeList);

    unsigned int lNodeListPos(1);
    for (I * i: mItems)
    {
      if (i->canWriteToXml())
      {
        if (lNodeListPos < lNodeList.size())
        {
          if (!i->writeToXml(lNodeList[lNodeListPos - 1], arErrorMessage))
            return false;

          ++lNodeListPos;
        }
        else
        {
          CDOMNode lNewConfigNode(arConfigNode, i->getXmlNodeName());

          if (!i->writeToXml(lNewConfigNode, arErrorMessage))
            return false;
        }
      }
    }
  }

  return true;
}

template<class I>
bool CPersistentConfigBunch<I>::get(typename std::list<I *>::const_iterator & arBeginIter,
  typename std::list<I *>::const_iterator & arEndIter) const
{
  if (!mItems.empty())
  {
    arBeginIter = mItems.cbegin();
    arEndIter = mItems.cend();

    return true;
  }
  else
    return false;
}

template<class I>
I * CPersistentConfigBunch<I>::get(unsigned int aIndex) const
{
  if (aIndex < mItems.size())
  {
    typename std::list<I *>::const_iterator i(mItems.begin());
    i.operator++(aIndex);

    return *i;
  }
  else
    return 0;
}


template<class I>
bool CPersistentConfigBunch<I>::contains(I * apItem) const
{
  if (apItem)
  {
    for (I * i: mItems)
    {
      if (i == apItem)
        return true;
    }
  }

  return false;
}

template<class I>
bool CPersistentConfigBunch<I>::add(I * apItem, std::wstring & arErrorMessage)
{
  if (apItem)
  {
    mItems.push_back(apItem);

    apItem->setParent(this);

    return true;
  }
  else
  {
    arErrorMessage = L"Cannot add null pointer as item";

    return false;
  }
}

template<class I>
bool CPersistentConfigBunch<I>::extract(I * aprItem)
{
  auto i(mItems.size());
  mItems.remove(aprItem);

  if (i > mItems.size())
  {
    aprItem->setParent(0);

    return true;
  }
  else
    return false;
}

template<class I>
bool CPersistentConfigBunch<I>::remove(I * & aprItem)
{
  if (extract(aprItem))
  {
    delete aprItem;
    aprItem = 0;

    return true;
  }
  else
    return false;
}

#endif
