// This file is part of the LosaXML library.
// Copyright (C) 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <LosaXML/DOMDocument.h>
#include <LosaXML/Utils.h> // for convertStr<>()
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

using namespace LosaXML;

// GenericException

EGeneric::EGeneric(const char * aWhat) throw()
  : inherited()
  , mWhat(aWhat)
{
}

EGeneric::~EGeneric() throw()
{
}

const char* EGeneric::what() const throw()
{
  return mWhat;
}

// RootNodeAlreadyPresent

ERootNodeAlreadyPresent::ERootNodeAlreadyPresent() throw()
  : inherited("Root node is already present")
{
}

ERootNodeAlreadyPresent::~ERootNodeAlreadyPresent() throw()
{
}

// CBaseTreeNode

CBaseDOMNode::CBaseDOMNode()
  : mName(L"")
  , mpParentNode(0)
  , mpTreeNode(0)
{
}

CBaseDOMNode::CBaseDOMNode(CBaseDOMNode & apParentNode, std::wstring aNodeName, std::wstring aNodeValue) throw (EGeneric)
  : mName(L"")
  , mpParentNode(&apParentNode)
  , mpTreeNode(0)
{
  if (apParentNode.mpTreeNode)
  {
    mpTreeNode = &apParentNode.mpTreeNode->add(aNodeName, aNodeValue);
  }
  else
    throw EGeneric("State of node inconsistent (parent node has no underlying ptree node)");
}

CBaseDOMNode::~CBaseDOMNode()
{
}

inline void CBaseDOMNode::checkUnderlyingTreeNode() const throw(EGeneric)
{
  if (!mpTreeNode)
    throw EGeneric("State of node inconsistent (no underlying ptree node)");
}

std::wstring CBaseDOMNode::getName() const
{
  return mName;
}

bool CBaseDOMNode::getValue(std::wstring & aValue, std::wstring & arErrorMessage) const
{
  return getValue<std::wstring>(aValue, arErrorMessage);
}

bool CBaseDOMNode::getValue(int & aValue, std::wstring & arErrorMessage) const
{
  return getValue<int>(aValue, arErrorMessage);
}

void CBaseDOMNode::reinitializeXMLNode(std::wstring aName,
                                        const CBaseDOMNode * apParentNode,
                                        CBoostTreeNode * apTreeNode)
{
  mName = aName;
  mpParentNode = apParentNode;
  mpTreeNode = apTreeNode;
}

template<typename T>
inline bool CBaseDOMNode::getValue(T & arValue, std::wstring & arErrorMessage) const
{
  try
  {
    checkUnderlyingTreeNode();

    arValue = mpTreeNode->get_value<T>();

    return true;
  }
  catch (EGeneric &e)
  {
    arErrorMessage = LosaXML::convertString<std::wstring, std::string>(e.what());
  }
  catch (boost::property_tree::ptree_bad_data &e)
  {
    arErrorMessage = LosaXML::convertString<std::wstring, std::string>(e.what());
  }
  catch (...)
  {
    arErrorMessage = L"Unknown error";
  }

  return false;
}

template<typename T>
inline bool CBaseDOMNode::getNodeValue(std::wstring aNodeName, T aDefaultValue, T & arNodeValue, std::wstring & arErrorMessage) const
{
  try
  {
    checkUnderlyingTreeNode();

    CBoostTreeNode & lNode(mpTreeNode->get_child(aNodeName));

    arNodeValue = lNode.get_value<T>();

    return true;
  }
  catch (EGeneric &e)
  {
    arErrorMessage = LosaXML::convertString<std::wstring, std::string>(e.what());
  }
  catch (boost::property_tree::ptree_bad_path &e)
  {
    //arErrorMessage = LosaXML::convertString<std::wstring, std::string>(e.what());

    arNodeValue = aDefaultValue;

    return true;
  }
  catch (boost::property_tree::ptree_bad_data &e)
  {
    arErrorMessage = LosaXML::convertString<std::wstring, std::string>(e.what());
  }
  catch (...)
  {
    arErrorMessage = L"Unknown error";
  }

  return false;
}

// CXMLAttribute

CXMLAttribute::CXMLAttribute()
  : inherited()
{}

CXMLAttribute::CXMLAttribute(CDOMNode & apParentNode, std::wstring aAttributeName, std::wstring aAttributeValue) throw (EGeneric)
  : inherited(apParentNode, aAttributeName, aAttributeValue)
{}

CXMLAttribute::~CXMLAttribute()
{}

// CXMLNode

CDOMNode::CDOMNode()
  : inherited()
{}

CDOMNode::CDOMNode(CDOMNode & apParentNode, std::wstring aNodeName, std::wstring aNodeValue) throw (EGeneric)
  : inherited(apParentNode, aNodeName, aNodeValue)
{}

CDOMNode::~CDOMNode()
{}

void CDOMNode::addNode(std::wstring aNodeName, std::wstring aNodeValue)
{
  checkUnderlyingTreeNode();

  mpTreeNode->add<std::wstring>(aNodeName, aNodeValue);
}

void CDOMNode::addNode(std::wstring aNodeName, int aNodeValue)
{
  checkUnderlyingTreeNode();

  mpTreeNode->add<int>(aNodeName, aNodeValue);
}

void CDOMNode::addNode(std::wstring aNodeName, unsigned int aNodeValue)
{
  checkUnderlyingTreeNode();

  mpTreeNode->add<unsigned int>(aNodeName, aNodeValue);
}

void CDOMNode::addNode(std::wstring aNodeName, float aNodeValue)
{
  checkUnderlyingTreeNode();

  mpTreeNode->add<float>(aNodeName, aNodeValue);
}

void CDOMNode::addNode(std::wstring aNodeName, bool aNodeValue)
{
  checkUnderlyingTreeNode();

  mpTreeNode->add<bool>(aNodeName, aNodeValue);
}

void CDOMNode::addNode(std::wstring aNodeName, wchar_t aNodeValue)
{
  checkUnderlyingTreeNode();

  mpTreeNode->add<wchar_t>(aNodeName, aNodeValue);
}

bool CDOMNode::hasNode(std::wstring aNodeName) const
{
  try
  {
    checkUnderlyingTreeNode();

    mpTreeNode->get_child(aNodeName);

    return true;
  }
  catch (EGeneric &e)
  {
    // something went wrong, so no node
  }
  catch (boost::property_tree::ptree_bad_path &e)
  {
    // node path does not exist, so no node
  }

  return false;
}

bool CDOMNode::getNode(std::wstring aNodeName, CDOMNode & aNode, std::wstring * arErrorMessage) const
{
  checkUnderlyingTreeNode();

  try
  {
    CBoostTreeNode & lNode(mpTreeNode->get_child(aNodeName));

    aNode.reinitializeXMLNode(
      aNodeName,
      this,
      &lNode
    );

    return true;
  }
  catch (boost::property_tree::ptree_bad_path &e)
  {
    if (arErrorMessage)
      *arErrorMessage = LosaXML::convertString<std::wstring, std::string>(e.what());
  }

  return false;
}

bool CDOMNode::getNodeValue(std::wstring aNodeName, std::wstring aDefaultValue, std::wstring & arNodeValue, std::wstring & arErrorMessage) const
{
  return inherited::getNodeValue<std::wstring>(aNodeName, aDefaultValue, arNodeValue, arErrorMessage);
}

bool CDOMNode::getNodeValue(std::wstring aNodeName, int aDefaultValue, int & arNodeValue, std::wstring & arErrorMessage) const
{
  return inherited::getNodeValue<int>(aNodeName, aDefaultValue, arNodeValue, arErrorMessage);
}

bool CDOMNode::getNodeValue(std::wstring aNodeName, unsigned int aDefaultValue, unsigned int & arNodeValue, std::wstring & arErrorMessage) const
{
  return inherited::getNodeValue<unsigned int>(aNodeName, aDefaultValue, arNodeValue, arErrorMessage);
}

bool CDOMNode::getNodeValue(std::wstring aNodeName, float aDefaultValue, float & arNodeValue, std::wstring & arErrorMessage) const
{
  return inherited::getNodeValue<float>(aNodeName, aDefaultValue, arNodeValue, arErrorMessage);
}

bool CDOMNode::getNodeValue(std::wstring aNodeName, bool aDefaultValue, bool & arNodeValue, std::wstring & arErrorMessage) const
{
  return inherited::getNodeValue<bool>(aNodeName, aDefaultValue, arNodeValue, arErrorMessage);
}

bool CDOMNode::getNodeValue(std::wstring aNodeName, wchar_t aDefaultValue, wchar_t & arNodeValue, std::wstring & arErrorMessage) const
{
  return inherited::getNodeValue<wchar_t>(aNodeName, aDefaultValue, arNodeValue, arErrorMessage);
}

bool CDOMNode::getNodes(std::wstring aNodeName, CXMLNodeList & aNodeList) const
{
  checkUnderlyingTreeNode();

  aNodeList.clear();
  int i(0);

  CBoostTreeNode::iterator lNodeIter(mpTreeNode->begin());
  while (lNodeIter != mpTreeNode->end())
  {
    std::wstring lNodeName(lNodeIter->first);

    if (lNodeName.compare(aNodeName) == 0)
    {
      CBoostTreeNode * lTreeNode(&lNodeIter->second);

      aNodeList.resize(i + 1);

      aNodeList[i].reinitializeXMLNode(
        aNodeName,
        this,
        lTreeNode
      );

      ++i;
    }

    ++lNodeIter;
  }

  return i > 0;
}

unsigned int CDOMNode::getNodeCount(std::wstring aNodeName)
{
  checkUnderlyingTreeNode();

  unsigned int i(0);

  CBoostTreeNode::iterator lNodeIter(mpTreeNode->begin());
  while (lNodeIter != mpTreeNode->end())
  {
    std::wstring lNodeName(lNodeIter->first);

    if (lNodeName.compare(aNodeName) == 0)
      ++i;

    ++lNodeIter;
  }

  return i;
}

bool CDOMNode::hasAttribute(const std::wstring & aAttributeName) const
{
  try
  {
    checkUnderlyingTreeNode();

    // note: boost property tree stores attributes as subnodes of <xmlattr>
    mpTreeNode->get_child(L"<xmlattr>." + aAttributeName);

    return true;
  }
  catch (EGeneric &e)
  {
    // something went wrong, so no attribute
  }
  catch (boost::property_tree::ptree_bad_path &e)
  {
    // attribute path does not exist, so no attribute
  }

  return false;
}

bool CDOMNode::getAttribute(std::wstring aAttributeName, CXMLAttribute & aAttribute, std::wstring & arErrorMessage) const
{
  checkUnderlyingTreeNode();

  try
  {
    // note: boost property tree stores attributes as subnodes of <xmlattr>
    CBoostTreeNode & lAttributeNode(mpTreeNode->get_child(L"<xmlattr>"));

    CBoostTreeNode & lNode(lAttributeNode.get_child(aAttributeName));

    aAttribute.reinitializeXMLNode(
      aAttributeName,
      this,
      &lNode
    );

    return true;
  }
  catch (boost::property_tree::ptree_bad_path &e)
  {
    arErrorMessage = LosaXML::convertString<std::wstring, std::string>(e.what());
  }

  return false;
}

template <typename T>
bool CDOMNode::checkAttributeValuePrivate(const std::wstring & aAttributeName, const T & aValue) const
{
  if (hasAttribute(aAttributeName))
  {
    std::wstring e;
    CXMLAttribute a;

    if (getAttribute(aAttributeName, a, e))
    {
      T v(aValue);
      if (a.getValue(v, e))
        return aValue == v;
      else
        return false;
    }
    else
      return false;
  }
  else
    return false;
}

bool CDOMNode::checkAttributeValue(const std::wstring & aAttributeName, const std::wstring & aValue) const
{
  return checkAttributeValuePrivate<std::wstring>(aAttributeName, aValue);
}

bool CDOMNode::checkAttributeValue(const std::wstring & aAttributeName, const int & aValue) const
{
  return checkAttributeValuePrivate<int>(aAttributeName, aValue);
}

template <typename T>
bool CDOMNode::getAttributeValuePrivate(const std::wstring & aAttributeName, const T & aDefaultValue, T & arAttributeValue, std::wstring & arErrorMessage) const
{
  if (hasAttribute(aAttributeName))
  {
    CXMLAttribute a;
    if (getAttribute(aAttributeName, a, arErrorMessage))
      return a.getValue(arAttributeValue, arErrorMessage);
    else
      return false;
  }
  else
  {
    arAttributeValue = aDefaultValue;

    return true;
  }
}

bool CDOMNode::getAttributeValue(const std::wstring & aAttributeName, const std::wstring & aDefaultValue, std::wstring & arAttributeValue, std::wstring & arErrorMessage) const
{
  return getAttributeValuePrivate<std::wstring>(aAttributeName, aDefaultValue, arAttributeValue, arErrorMessage);
}

bool CDOMNode::getAttributeValue(const std::wstring & aAttributeName, const int & aDefaultValue, int & arAttributeValue, std::wstring & arErrorMessage) const
{
  return getAttributeValuePrivate<int>(aAttributeName, aDefaultValue, arAttributeValue, arErrorMessage);
}

bool CDOMNode::getAttributeValue(const std::wstring & aAttributeName, const unsigned int & aDefaultValue, unsigned int & arAttributeValue, std::wstring & arErrorMessage) const
{
  return getAttributeValuePrivate<unsigned int>(aAttributeName, aDefaultValue, arAttributeValue, arErrorMessage);
}

// CXMLParser

CDOMRootNode::CDOMRootNode()
  : inherited()
{
}

CDOMRootNode::CDOMRootNode(CDOMDocument & arDocument, std::wstring aNodeName) throw (ERootNodeAlreadyPresent)
  : inherited()
{
  if (!arDocument.mpXmlStructure->empty())
    throw ERootNodeAlreadyPresent();

  boost::optional<CBoostTreeNode &> lRootNode = arDocument.mpXmlStructure->get_child_optional(aNodeName);
  if (lRootNode.is_initialized())
    throw ERootNodeAlreadyPresent();

  reinitializeXMLNode(
    aNodeName,
    0,
    &arDocument.mpXmlStructure->add(aNodeName, L"")
  );
}

// CXMLParser

CDOMDocument::CDOMDocument()
  : mpXmlStructure(new CDOMNode::CBoostTreeNode())
{
}

CDOMDocument::~CDOMDocument()
{
  delete mpXmlStructure;
}

bool CDOMDocument::read(std::string aXmlFilename, CDOMRootNode & arRootNode, std::wstring & rErrorMessage)
{
  try
  {
    // the following 3 lines are adopted from boost::property_tree::xml_parser::read_xml(const std::string & filename, ...)
    std::basic_ifstream<CDOMNode::CBoostTreeNode::key_type::value_type> stream(aXmlFilename.c_str());

    if (!stream)
      BOOST_PROPERTY_TREE_THROW(boost::property_tree::xml_parser::xml_parser_error("cannot open file", aXmlFilename, 0));

    stream.imbue(std::locale());

    // try to skip UTF-8 encoded Unicode BOM as property tree cannot read it
    // https://svn.boost.org/trac/boost/ticket/1678
    // https://svn.boost.org/trac/boost/ticket/8836
    wchar_t c;
    if (stream.get(c))
    {
      if (c == 0xEF)
      {
        if (stream.get(c))
        {
          if (c == 0xBB)
          {
            if (stream.get(c))
            {
              if (c == 0xBF)
              {
                // do nothing as we extracted Unicode BOM already
              }
              else
                stream.unget();
            }
          }
          else
            stream.unget();
        }
      }
      else
        stream.unget();
    }

    boost::property_tree::xml_parser::read_xml(stream, *mpXmlStructure, boost::property_tree::xml_parser::no_comments);

    arRootNode.reinitializeXMLNode(
      mpXmlStructure->front().first,
      0,
      &mpXmlStructure->front().second
    );

    return true;
  }
  catch (boost::property_tree::xml_parser::xml_parser_error &e)
  {
    rErrorMessage = LosaXML::convertString<std::wstring, std::string>(e.what());
  }

  return false;
}

bool CDOMDocument::write(std::string aXmlFilename, std::wstring & rErrorMessage)
{
  try
  {
    boost::property_tree::xml_parser::write_xml(aXmlFilename, *mpXmlStructure, std::locale(),
      boost::property_tree::xml_parser::xml_writer_settings<CDOMNode::CBoostTreeNode::key_type>(' ', 2));

    return true;
  }
  catch (boost::property_tree::xml_parser::xml_parser_error &e)
  {
    rErrorMessage = LosaXML::convertString<std::wstring, std::string>(e.what());
  }

  return false;
}
